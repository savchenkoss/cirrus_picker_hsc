#!/usr/bin/env python

from os import makedirs
import argparse
from pathlib import Path
import numpy as np
from scipy.ndimage import median_filter
from scipy.ndimage import gaussian_filter
from scipy.ndimage import zoom
from scipy.ndimage import label
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from astropy.io import fits
import paramiko

field_size = 2143
iris_size = 304
path_to_data_on_server = Path("/mnt/data/S82_HSCSSP_rescale")
band_list = ["r"]  # ["g", "r", "i"]


def download(client, field_name):
    """
    Downloads both original and cleaned fields
    """
    if not Path("cache").exists():
        makedirs("cache")
    # List of all images to be downloaded
    file_names = [path_to_data_on_server/f"{field_name}_combined_mask.fits",
                  path_to_data_on_server/f"{field_name}_cleaned_r.fits",
                  path_to_data_on_server/f"{field_name}_regions.fits"]
    # Path(f"/mnt/data/S82_IAC_r_only_IRIS/{field_name}_IRIS.fits")
    tags = ["mask", "cleaned", "regions"]  # "iris"
    for band in band_list:
        file_names.append(path_to_data_on_server/f"{field_name}_coadd+bg_{band}.fits")
        tags.append(band)

    # Download images
    downloaded_data = {}
    for remote_path, tag in zip(file_names, tags):
        local_path = Path("cache") / remote_path.name
        if Path(local_path).exists():
            print(f"Using cached version of image '{tag}' for field {field_name}")
            try:
                downloaded_data[tag] = fits.getdata(local_path)
            except OSError:
                print("Having some troubles with that. Using zeros")
                downloaded_data[tag] = np.zeros((field_size, field_size))
        else:
            try:
                print(f"Downloading image '{tag}' for field {field_name}")
                client.get(str(remote_path), str(local_path))
                downloaded_data[tag] = fits.getdata(local_path)
            except (IOError, FileNotFoundError, OSError):
                print(f"{field_name} field in {band} band doed not exist")
                downloaded_data[tag] = np.zeros((field_size, field_size))
    return downloaded_data


class Picker(object):
    def __init__(self, regions, orig_data, cleaned_data, cleaned_smoothed_data, cleaned_surface_brightness, old_result):
        self.layers = {"regions": regions,
                       "orig_data": orig_data,
                       "cleanded_data": cleaned_data,
                       "cleaned_smoothed_data": cleaned_smoothed_data,
                       "cleaned_surface_brightness": cleaned_surface_brightness}
        self.layer_idx = 0
        self.lim_mag = 29
        self.contrast_factor = 8
        self.layer_plot_instance = None
        self.segm_plot_instance = None
        self.selected_labels = []
        self.segments, n_labels = label(regions)
        if old_result is not None:
            self.selected_labels = list(set(self.segments[np.where(old_result == 1)]))
        self.segm_to_plot = np.zeros_like(regions) * np.nan
        self.segm_plot_instance = None
        self.segm_to_plot = np.copy(self.layers["regions"])
        for lbl in self.selected_labels:
            if lbl == 0:
                continue
            self.segm_to_plot[self.segments == lbl] = 10
        self.fig, self.ax = plt.subplots()
        self.available_sizes = [1000, 900, 800,
                                750, 700, 650, 600, 550, 500, 450, 400,
                                375, 350, 325, 300, 275, 250, 225, 200,
                                175, 150, 125, 100, 75, 50,
                                45, 40, 35, 30, 25, 20, 15, 10]
        self.size = 200
        self.fig.canvas.mpl_connect('motion_notify_event', self.motion)
        self.fig.canvas.mpl_connect('button_press_event', self.on_click)
        self.fig.canvas.mpl_connect('scroll_event', self.on_scroll)
        self.fig.canvas.mpl_connect('key_press_event', self.on_press)

        self.toggle_layer()

        self.rect = Rectangle((50, 100), self.size, self.size, linewidth=1, edgecolor='b', facecolor='none')
        self.ax.add_patch(self.rect)
        self.cursor_state = "picking"
        # Draw central field borders
        data_size = regions.shape[0]
        field_size = data_size // 3
        x = [field_size, 2*field_size, 2*field_size, field_size, field_size]
        y = [field_size, field_size, 2*field_size, 2*field_size, field_size]
        self.ax.plot(x, y, linestyle=":", color="r")
        self.fig.tight_layout()

    def toggle_layer(self, roll=False, new_layer=None):
        if roll is True:
            self.layer_idx += 1
            self.layer_idx = self.layer_idx % len(list(self.layers.keys()))
        elif new_layer is not None and (new_layer <= len(list(self.layers.keys()))):
            self.layer_idx = new_layer-1
        if self.layer_plot_instance is not None:
            self.layer_plot_instance.remove()
        key = list(self.layers.keys())[self.layer_idx]
        to_plot = self.layers[key].copy()
        if key == "cleaned_surface_brightness":
            to_plot[to_plot > self.lim_mag] = np.nan
            to_plot[to_plot < self.lim_mag] = 5 / 20
            self.ax.set_title(f"surf bri < {self.lim_mag:1.2f}")
        else:
            self.ax.set_title(key)
        vmin = -0.1
        vmax = 8 / 8
        if "orig_data" in key:
            print(self.contrast_factor)
            vmax = 8 / self.contrast_factor
        self.layer_plot_instance = self.ax.imshow(to_plot, origin="lower",
                                                  interpolation="nearest",
                                                  vmin=vmin, vmax=vmax)
        if key == "regions":
            if self.segm_plot_instance is not None:
                self.segm_plot_instance.remove()
                self.segm_plot_instance = None
            self.segm_plot_instance = self.ax.imshow(self.segm_to_plot, interpolation='none', vmin=0,
                                                     vmax=2, origin='lower')
        else:
            if self.segm_plot_instance is not None:
                self.segm_plot_instance.remove()
                self.segm_plot_instance = None
        self.fig.canvas.draw()

    def toggle_cursor_state(self):
        if self.cursor_state == "picking":
            self.cursor_state = "drawing"
            self.rect.set_edgecolor("r")
        else:
            self.cursor_state = "picking"
            self.rect.set_edgecolor("b")
        self.rect.figure.canvas.draw()

    def motion(self, event):
        if event.xdata is not None and event.ydata is not None:
            self.rect.set_xy((event.xdata-self.size//2, event.ydata-self.size//2))
            self.rect.figure.canvas.draw()

    def on_press(self, event):
        if event.key == "up":
            self.lim_mag += 0.25
            self.toggle_layer(roll=False)
            return
        if event.key == "down":
            print(self.lim_mag)
            self.lim_mag -= 0.25
            self.toggle_layer(roll=False)
            return
        if event.key == " ":
            self.toggle_layer(roll=True)
        if event.key == "d":
            self.toggle_cursor_state()
        if event.key == "+":
            self.contrast_factor += 1
            self.toggle_layer(roll=False)
        if event.key == "-":
            self.contrast_factor = max(1, self.contrast_factor - 1)
            self.toggle_layer(roll=False)

        try:
            k = int(event.key)
            self.toggle_layer(new_layer=k)
        except ValueError:
            pass

    def on_scroll(self, event):
        index = self.available_sizes.index(self.size)
        if event.button == "up" and index > 0:
            self.size = self.available_sizes[index - 1]
        elif event.button == "down" and index < (len(self.available_sizes) - 1):
            self.size = self.available_sizes[index + 1]
        self.rect.set_width(self.size)
        self.rect.set_height(self.size)
        self.rect.figure.canvas.draw()

    def on_click(self, event):
        x_start = int(event.xdata) - self.size // 2
        x_end = int(event.xdata) + self.size // 2
        y_start = int(event.ydata) - self.size // 2
        y_end = int(event.ydata) + self.size // 2
        if self.cursor_state == 'picking':
            labels = list(set(self.segments[y_start: y_end, x_start: x_end].ravel()))
            if event.button == 1:
                # Left button, we are adding labels
                for lbl in labels:
                    if lbl not in self.selected_labels:
                        self.selected_labels.extend(labels)
            elif event.button == 3:
                # Right button, we are removing labels
                for lbl in labels:
                    if lbl in self.selected_labels:
                        self.selected_labels.remove(lbl)
            self.segm_to_plot = np.copy(self.layers["regions"])
            for lbl in self.selected_labels:
                if lbl == 0:
                    continue
                self.segm_to_plot[self.segments == lbl] = 10
            self.toggle_layer(roll=False)
        else:
            if event.button == 1:
                # Left button, removing data
                self.layers["regions"][y_start: y_end, x_start: x_end] = 0
            elif event.button == 3:
                # Right button, adding data
                self.layers["regions"][y_start: y_end, x_start: x_end] = 5
            self.segm_to_plot = np.copy(self.layers["regions"])
            # Backup selected pixels
            backup = np.zeros_like(self.layers["regions"])
            for lbl in self.selected_labels:
                if lbl == 0:
                    continue
                backup[self.segments == lbl] = 1
            self.segments, _ = label(self.layers["regions"])
            self.selected_labels = list(set(self.segments[np.where(backup == 1)].ravel()))
            self.segm_to_plot = np.copy(self.layers["regions"])
            for lbl in self.selected_labels:
                if lbl == 0:
                    continue
                self.segm_to_plot[self.segments == lbl] = 10
            self.toggle_layer(roll=False)

    def save_results(self, name):
        results = np.zeros_like(self.layers["regions"])
        for lbl in self.selected_labels:
            if lbl == 0:
                continue
            results[self.segments == lbl] = 1
        results[(results == 0) * (self.segments > 1)] = 2
        if not Path("results").exists():
            makedirs("results")
        out_path = f"results/{name}.fits"
        data_to_save = results[field_size:2*field_size, field_size:2*field_size]
        data_to_save = zoom(data_to_save, 5, order=0).astype(np.int8)
        fits.PrimaryHDU(data=data_to_save).writeto(out_path, overwrite=True)


def main(args):
    # Load coordinates
    names = []
    ra_all = []
    dec_all = []
    for line in open("s82_coords.dat"):
        if line.startswith("#"):
            continue
        names.append(line.split()[0])
        ra_all.append(float(line.split()[1]))
        dec_all.append(float(line.split()[2]))
    ra_all = np.array(ra_all)
    dec_all = np.array(dec_all)
    field_idx = names.index(args.field)
    field_ra = ra_all[field_idx]
    field_dec = dec_all[field_idx]

    # Download fields
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(hostname='82.179.36.175', port=50001)
    ftp_client = ssh_client.open_sftp()

    # Join fields in a single field
    # field_size_iris = 3 * 304
    big_field = {}
    for band in band_list:
        big_field[band] = np.zeros((3*field_size, 3*field_size), dtype=np.float32)
    big_field["mask"] = np.zeros((3*field_size, 3*field_size), dtype=np.float32)
    # big_field["iris"] = np.zeros((field_size_iris, field_size_iris), dtype=np.float32)
    big_field["cleaned"] = np.zeros((3*field_size, 3*field_size), dtype=np.float32)
    big_field["regions"] = np.zeros((3*field_size, 3*field_size), dtype=np.float32)

    # Download all fields
    for shift_i in (-1, 0, 1):
        for shift_j in (-1, 0, 1):
            ra_target = field_ra - shift_i * 0.5
            dec_target = field_dec - shift_j * 0.5
            dists = np.hypot(ra_all-ra_target, dec_all-dec_target)
            idx_target = np.argmin(dists)
            if dists[idx_target] < 0.1:
                downloaded_data = download(ftp_client, names[idx_target])
                for tag in downloaded_data.keys():
                    y_size, x_size = downloaded_data[tag].shape
                    x_start = x_size + x_size*shift_i
                    y_start = y_size - y_size*shift_j
                    big_field[tag][y_start:y_start+y_size, x_start:x_start+x_size] = downloaded_data[tag]

    # If some results do exists already, load them
    results_path = Path(f"results/{args.field}.fits")
    if results_path.exists():
        old_result = np.zeros((3*field_size, 3*field_size), dtype=np.float32)
        old_result[field_size:2*field_size, field_size:2*field_size] = zoom(fits.getdata(results_path), 1/5, order=0)
    else:
        old_result = None

    # Smooth the data a bit
    cleaned_smoothed = gaussian_filter(median_filter(big_field["cleaned"], 5), 4)

    # Compute the surface brightness on a cleaned region
    pixel_scale = 5 * 0.168  # In arcsec per pixel. 0.168 is an original HSC scale, and 5 is a zoom factor
    pixel_area = pixel_scale ** 2
    mag_zpt = 27
    cleaned_surface_brightness = mag_zpt - 2.5 * np.log10(cleaned_smoothed) + 2.5*np.log10(pixel_area)

    if args.save_fields:
        for band in band_list:
            fits.PrimaryHDU(data=big_field[band]).writeto(f"big_orig_{band}.fits", overwrite=True)
        fits.PrimaryHDU(data=big_field["cleaned"]).writeto("big_cleaned.fits", overwrite=True)
        fits.PrimaryHDU(data=cleaned_smoothed).writeto("big_cleaned_smoothed.fits", overwrite=True)
        fits.PrimaryHDU(data=cleaned_surface_brightness).writeto("cleaned_surface_brightness.fits", overwrite=True)
        fits.PrimaryHDU(data=big_field["regions"]).writeto("regions.fits", overwrite=True)

    p = Picker(big_field["regions"], big_field["r"], big_field["cleaned"], cleaned_smoothed,
               cleaned_surface_brightness, old_result)
    plt.show()
    p.save_results(args.field)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--field")
    parser.add_argument("--mag", type=float, default=29.0)
    parser.add_argument("--save-fields", action="store_true", default=False,
                        help="Save FITS files with combined fields")
    args = parser.parse_args()
    main(args)
